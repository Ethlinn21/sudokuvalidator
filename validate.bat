@echo off
if not exist %~dp0target\SudokuValidator-1.0-SNAPSHOT.jar (
    echo "You need to build a jarfile by maven clean install first"
)
set path=%PATH%;%JAVA_HOME%\bin;
set /p filename="Enter filename with extension: "
java -jar %~dp0target\SudokuValidator-1.0-SNAPSHOT.jar %filename%
pause
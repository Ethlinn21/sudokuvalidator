package org.ubs.interview.sudoku.validator.parser;

import org.junit.jupiter.api.Test;
import org.ubs.interview.sudoku.validator.exception.IncorrectSudokuSizeException;
import org.ubs.interview.sudoku.validator.exception.SudokuErrorMessage;

import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

class SudokuParserTest {

    @Test
    void shouldNotParseSudokuDueToTheInvalidChar() {
        //given
        String path = Paths.get("src", "test", "resources", "invalid_char.txt").toFile().getAbsolutePath();

        //when, then
        assertThrows(NumberFormatException.class, () -> SudokuParser.parse(path));
    }

    @Test
    void shouldNotParseSudokuDueToTooFewColumns() {
        //given
        String path = Paths.get("src", "test", "resources", "too_few_columns_in_row.txt")
                .toFile().getAbsolutePath();

        //when, then
        assertThrows(IncorrectSudokuSizeException.class, () -> SudokuParser.parse(path),
                SudokuErrorMessage.INCORRECT_ROW_LENGTH.getErrorMessage());
    }

    @Test
    void shouldNotParseSudokuDueToTooFewRows() {
        //given
        String path = Paths.get("src", "test", "resources", "too_few_rows.txt")
                .toFile().getAbsolutePath();

        //when, then
        assertThrows(IncorrectSudokuSizeException.class, () -> SudokuParser.parse(path),
                SudokuErrorMessage.UNEXPECTED_NUMBER_OF_ROWS.getErrorMessage());
    }

    @Test
    void shouldNotParseSudokuDueToTooManyColumns() {
        //given
        String path = Paths.get("src", "test", "resources", "too_many_columns.txt")
                .toFile().getAbsolutePath();

        //when, then
        assertThrows(IncorrectSudokuSizeException.class, () -> SudokuParser.parse(path),
                SudokuErrorMessage.INCORRECT_ROW_LENGTH.getErrorMessage());
    }

    @Test
    void shouldNotParseSudokuDueToTooManyRows() {
        //given
        String path = Paths.get("src", "test", "resources", "too_many_rows.txt")
                .toFile().getAbsolutePath();

        //when, then
        assertThrows(IncorrectSudokuSizeException.class, () -> SudokuParser.parse(path),
                SudokuErrorMessage.UNEXPECTED_NUMBER_OF_ROWS.getErrorMessage());
    }

    @Test
    void shouldParseIncorrectSudokuWithValidColumnsAndRowsLengthAndValidNumericCharacters() {
        //given
        String path = Paths.get("src", "test", "resources", "invalid_sudoku.txt").toFile().getAbsolutePath();

        //when, then
        assertDoesNotThrow(() -> SudokuParser.parse(path));
    }
}
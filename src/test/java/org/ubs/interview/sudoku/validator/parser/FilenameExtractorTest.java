package org.ubs.interview.sudoku.validator.parser;

import org.junit.jupiter.api.Test;
import org.ubs.interview.sudoku.validator.exception.SudokuErrorMessage;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class FilenameExtractorTest {

    @Test
    void shouldThrowExceptionOnFilenameMissing() {
        assertThrows(IOException.class, FilenameExtractor::getFileName,
                SudokuErrorMessage.NO_FILENAME_DEFINED.getErrorMessage());
    }

    @Test
    void shouldThrowExceptionOnWrongFileExtension() {
        assertThrows(IOException.class, () -> FilenameExtractor.getFileName("invalid_extension.csv"),
                SudokuErrorMessage.NO_FILENAME_DEFINED.getErrorMessage());
    }

    @Test
    void shouldGetCorrectFilename() throws IOException {
        //given
        String filename = "abc.txt";

        //when
        String checkedFilename = FilenameExtractor.getFileName(filename);

        //then
        assertEquals(filename, checkedFilename);
    }

    @Test
    void shouldGetOnlyFirstFilenameWhenMoreAreGiven() throws IOException {
        //given
        String firstFilename = "abc.txt";

        //when
        String checkedFilename = FilenameExtractor.getFileName(firstFilename, "other.txt", "more.txt");

        //then
        assertEquals(firstFilename, checkedFilename);
    }
}
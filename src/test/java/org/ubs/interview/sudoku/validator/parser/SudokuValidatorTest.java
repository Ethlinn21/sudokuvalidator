package org.ubs.interview.sudoku.validator.parser;


import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.ubs.interview.sudoku.validator.model.Sudoku;

import java.util.Arrays;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SudokuValidatorTest {

    @ParameterizedTest
    @MethodSource("validSudokus")
    void shouldValidateCorrectSudokusWithoutThrowingException(Sudoku sudoku) {
        //given
        SudokuValidator sudokuValidator = new SudokuValidator(sudoku);

        //when
        boolean isValid = sudokuValidator.isCorrect();

        //then
        assertTrue(isValid);
    }

    @ParameterizedTest
    @MethodSource("invalidSudokus")
    void shouldThrowExceptionOnInvalidSudoku(Sudoku sudoku) {
        //given
        SudokuValidator sudokuValidator = new SudokuValidator(sudoku);

        //when
        boolean isValid = sudokuValidator.isCorrect();

        //then
        assertFalse(isValid);
    }

    static Stream<Arguments> validSudokus() {
        return Stream.of(
                Arguments.of(Sudoku.of(Arrays.asList(
                        8, 6, 7, 3, 1, 9, 2, 5, 4,
                        2, 4, 5, 7, 8, 6, 9, 3, 1,
                        9, 1, 3, 5, 4, 2, 6, 7, 8,
                        4, 7, 2, 9, 6, 3, 1, 8, 5,
                        3, 8, 1, 4, 2, 5, 7, 6, 9,
                        5, 9, 6, 8, 7, 1, 3, 4, 2,
                        7, 3, 8, 2, 9, 4, 5, 1, 6,
                        6, 5, 9, 1, 3, 8, 4, 2, 7,
                        1, 2, 4, 6, 5, 7, 8, 9, 3
                ))),
                Arguments.of(Sudoku.of(Arrays.asList(
                        6, 7, 9, 5, 1, 8, 2, 4, 3,
                        5, 4, 3, 7, 2, 9, 6, 1, 8,
                        8, 2, 1, 6, 3, 4, 9, 5, 7,
                        7, 9, 4, 3, 5, 2, 1, 8, 6,
                        3, 5, 8, 4, 6, 1, 7, 2, 9,
                        2, 1, 6, 8, 9, 7, 5, 3, 4,
                        4, 8, 5, 2, 7, 6, 3, 9, 1,
                        9, 6, 2, 1, 8, 3, 4, 7, 5,
                        1, 3, 7, 9, 4, 5, 8, 6, 2
                ))),
                Arguments.of(Sudoku.of(Arrays.asList(
                        1, 2, 3, 4, 5, 6, 7, 8, 9,
                        4, 5, 6, 7, 8, 9, 1, 2, 3,
                        7, 8, 9, 1, 2, 3, 4, 5, 6,
                        2, 3, 1, 5, 6, 4, 8, 9, 7,
                        5, 6, 4, 8, 9, 7, 2, 3, 1,
                        8, 9, 7, 2, 3, 1, 5, 6, 4,
                        3, 1, 2, 6, 4, 5, 9, 7, 8,
                        6, 4, 5, 9, 7, 8, 3, 1, 2,
                        9, 7, 8, 3, 1, 2, 6, 4, 5
                ))));
    }

    static Stream<Arguments> invalidSudokus() {
        return Stream.of(
                Arguments.of(Sudoku.of(Arrays.asList(
                        8, 6, 7, 3, 1, 9, 2, 5, 5,
                        2, 4, 5, 7, 8, 6, 9, 3, 1,
                        9, 1, 3, 5, 4, 2, 6, 7, 8,
                        4, 7, 2, 9, 6, 3, 1, 8, 5,
                        3, 8, 1, 4, 2, 5, 7, 6, 9,
                        5, 9, 6, 8, 7, 1, 3, 4, 2,
                        7, 3, 8, 2, 9, 4, 5, 1, 6,
                        6, 5, 9, 1, 3, 8, 4, 2, 7,
                        1, 2, 4, 6, 5, 7, 8, 9, 3
                ))),
                Arguments.of(Sudoku.of(Arrays.asList(
                        6, 7, 9, 5, 1, 8, 2, 4, 3,
                        5, 4, 3, 7, 2, 9, 6, 1, 8,
                        8, 2, 1, 6, 3, 4, 9, 5, 7,
                        7, 9, 4, 3, 5, 2, 1, 8, 6,
                        3, 5, 8, 4, 6, 1, 7, 2, 9,
                        2, 1, 6, 8, 9, 7, 5, 3, 4,
                        4, 8, 5, 2, 7, 6, 3, 9, 1,
                        9, 6, 2, 1, 8, 3, 4, 7, 5,
                        1, 3, 7, 9, 1, 5, 8, 6, 2
                ))),
                Arguments.of(Sudoku.of(Arrays.asList(
                        1, 2, 3, 4, 5, 6, 7, 8, 9,
                        4, 5, 6, 7, 8, 9, 1, 2, 3,
                        7, 8, 1, 1, 2, 3, 4, 5, 6,
                        2, 3, 1, 5, 6, 4, 8, 9, 7,
                        5, 6, 4, 8, 9, 7, 2, 3, 1,
                        8, 9, 7, 2, 3, 1, 5, 6, 4,
                        3, 1, 2, 6, 4, 5, 9, 7, 8,
                        6, 4, 5, 9, 7, 8, 3, 1, 2,
                        9, 7, 8, 3, 1, 2, 6, 4, 5
                ))));
    }
}
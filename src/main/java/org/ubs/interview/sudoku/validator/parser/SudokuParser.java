package org.ubs.interview.sudoku.validator.parser;

import lombok.experimental.UtilityClass;
import org.ubs.interview.sudoku.validator.exception.IncorrectSudokuSizeException;
import org.ubs.interview.sudoku.validator.model.Sudoku;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@UtilityClass
public class SudokuParser {

    public Sudoku parse(String filename) throws IOException, IncorrectSudokuSizeException {
        List<Integer> numbers = parseFile(filename);
        return Sudoku.of(numbers);
    }

    private List<Integer> parseFile(String fileName) throws IOException, IncorrectSudokuSizeException {
        List<Integer> numbers = new ArrayList<>(Sudoku.BOARD_LENGTH * Sudoku.BOARD_LENGTH);
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            String line;
            while ((line = reader.readLine()) != null) {
                numbers.addAll(parseRow(line));
            }
        }
        checkSize(numbers.size(), Sudoku.BOARD_LENGTH * Sudoku.BOARD_LENGTH,
                IncorrectSudokuSizeException::ofIncorrectNumberOfRows);
        return numbers;
    }

    private List<Integer> parseRow(String line) throws IncorrectSudokuSizeException {
        String[] numbers = line.split(",");
        checkSize(numbers.length, Sudoku.BOARD_LENGTH, IncorrectSudokuSizeException::ofIncorrectRowLength);
        return Arrays.stream(numbers)
                .map(Integer::valueOf)
                .collect(Collectors.toList());
    }

    private void checkSize(int actualSize, int expectedSize, Supplier<IncorrectSudokuSizeException> sudokuSizeExceptionSupplier)
            throws IncorrectSudokuSizeException {
        if (actualSize != expectedSize) {
            throw sudokuSizeExceptionSupplier.get();
        }
    }
}

package org.ubs.interview.sudoku.validator.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum SudokuErrorMessage {
    INCORRECT_ROW_LENGTH("One or more of Sudoku rows had invalid length"),
    UNEXPECTED_NUMBER_OF_ROWS("There was unexpected number of Sudoku rows"),
    NO_FILENAME_DEFINED("No filename was defined as input"), UNEXPECTED_FILE_EXTENSION("Unexpected file extension");

    private final String errorMessage;
}

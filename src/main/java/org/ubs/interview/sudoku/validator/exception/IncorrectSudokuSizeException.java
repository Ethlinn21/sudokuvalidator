package org.ubs.interview.sudoku.validator.exception;

public final class IncorrectSudokuSizeException extends Exception {

    public IncorrectSudokuSizeException(String message) {
        super(message);
    }

    public static IncorrectSudokuSizeException ofIncorrectNumberOfRows() {
        return new IncorrectSudokuSizeException(SudokuErrorMessage.UNEXPECTED_NUMBER_OF_ROWS.getErrorMessage());
    }

    public static IncorrectSudokuSizeException ofIncorrectRowLength() {
        return new IncorrectSudokuSizeException(SudokuErrorMessage.INCORRECT_ROW_LENGTH.getErrorMessage());
    }
}

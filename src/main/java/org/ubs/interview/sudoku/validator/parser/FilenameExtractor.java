package org.ubs.interview.sudoku.validator.parser;

import lombok.experimental.UtilityClass;
import org.ubs.interview.sudoku.validator.exception.SudokuErrorMessage;

import java.io.IOException;

@UtilityClass
public class FilenameExtractor {
    private static final String EXPECTED_EXTENSION = ".txt";

    public String getFileName(String... inputFilename) throws IOException {
        if (inputFilename.length == 0) {
            throw new IOException(SudokuErrorMessage.NO_FILENAME_DEFINED.getErrorMessage());
        }
        String filename = inputFilename[0];
        if (!getExtension(filename).equals(EXPECTED_EXTENSION)) {
            throw new IOException(SudokuErrorMessage.UNEXPECTED_FILE_EXTENSION.getErrorMessage());
        }
        return filename;
    }

    private static String getExtension(String filename) {
        return filename.substring(filename.lastIndexOf("."));
    }
}

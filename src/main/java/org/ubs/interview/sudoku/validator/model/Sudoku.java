package org.ubs.interview.sudoku.validator.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Value;

import java.util.List;

@Value
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Sudoku {
    public static final int BOARD_LENGTH = 9;
    public static final int SQUARE_LENGTH = 3;
    int[][] board;

    public static Sudoku of(List<Integer> numbers) {
        int rowNumber = -1;
        int[][] sudokuTemp = new int[BOARD_LENGTH][BOARD_LENGTH];
        for (int currentElementIndex = 0; currentElementIndex < BOARD_LENGTH * BOARD_LENGTH; currentElementIndex++) {
            if (currentElementIndex % BOARD_LENGTH == 0) {
                rowNumber++;
            }
            sudokuTemp[rowNumber][currentElementIndex % BOARD_LENGTH] = numbers.get(currentElementIndex);
        }
        return new Sudoku(sudokuTemp);
    }
}

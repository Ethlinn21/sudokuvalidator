package org.ubs.interview.sudoku.validator;

import org.ubs.interview.sudoku.validator.model.Sudoku;
import org.ubs.interview.sudoku.validator.parser.FilenameExtractor;
import org.ubs.interview.sudoku.validator.parser.SudokuParser;
import org.ubs.interview.sudoku.validator.parser.SudokuValidator;

public class App {

    public static void main(String... args) {
        Sudoku sudoku;
        try {
            String filename = FilenameExtractor.getFileName(args);
            sudoku = SudokuParser.parse(filename);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.exit(-1);
            return;
        }
        SudokuValidator sudokuValidator = new SudokuValidator(sudoku);
        if (sudokuValidator.isCorrect()) {
            System.out.println("Sudoku was correct");
            System.exit(0);
        } else {
            System.out.println("Sudoku was not correct");
            System.exit(-1);
        }

    }
}

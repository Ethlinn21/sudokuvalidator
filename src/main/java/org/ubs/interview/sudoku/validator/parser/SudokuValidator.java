package org.ubs.interview.sudoku.validator.parser;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Value;
import org.ubs.interview.sudoku.validator.model.Sudoku;

import java.util.HashSet;
import java.util.Set;


@Value
@Getter(AccessLevel.PRIVATE)
public class SudokuValidator {
    int[][] board;

    public SudokuValidator(Sudoku sudoku) {
        this.board = sudoku.getBoard();
    }

    public boolean isCorrect() {
        for (int rowNumber = 0; rowNumber < Sudoku.BOARD_LENGTH; rowNumber++) {
            if (!isRowValid(rowNumber)) {
                return false;
            }
            for (int columnNumber = 0; columnNumber < Sudoku.BOARD_LENGTH; columnNumber++) {
                if (isSquareApex(rowNumber, columnNumber) && !isSquareValid(rowNumber, columnNumber)) {
                    return false;
                }
                if (!isColumnValid(columnNumber)) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean isSquareApex(int rowNumber, int columnNumber) {
        return rowNumber % Sudoku.SQUARE_LENGTH == 0 && columnNumber % Sudoku.SQUARE_LENGTH == 0;
    }

    private boolean isSquareValid(int currentRow, int currentColumn) {
        Set<Integer> seemNumbers = new HashSet<>();
        for (int rowNumber = currentRow; rowNumber < currentRow + Sudoku.SQUARE_LENGTH; rowNumber++) {
            for (int columnNumber = currentColumn; columnNumber < currentColumn + Sudoku.SQUARE_LENGTH; columnNumber++) {
                int currentNumber = board[rowNumber][columnNumber];
                if (seemNumbers.contains(currentNumber)) {
                    return false;
                }
                seemNumbers.add(currentNumber);
            }
        }
        return true;
    }

    private boolean isColumnValid(int currentColumn) {
        Set<Integer> seenNumbers = new HashSet<>();
        for (int rowNumber = 0; rowNumber < Sudoku.BOARD_LENGTH; rowNumber++) {
            int currentNumber = board[rowNumber][currentColumn];
            if (seenNumbers.contains(currentNumber)) {
                return false;
            }
            seenNumbers.add(currentNumber);
        }
        return true;
    }

    private boolean isRowValid(int currentRow) {
        Set<Integer> seenNumbers = new HashSet<>();
        for (int columnNumber = 0; columnNumber < Sudoku.BOARD_LENGTH; columnNumber++) {
            int currentNumber = board[currentRow][columnNumber];
            if (seenNumbers.contains(currentNumber)) {
                return false;
            }
            seenNumbers.add(currentNumber);
        }
        return true;
    }
}

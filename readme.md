This app can validate sudoku from csv-format .txt file:

1,2,3,4,5,6,7,8,9
1,2,3,4,5,6,7,8,9
1,2,3,4,5,6,7,8,9
1,2,3,4,5,6,7,8,9
1,2,3,4,5,6,7,8,9
1,2,3,4,5,6,7,8,9
1,2,3,4,5,6,7,8,9
1,2,3,4,5,6,7,8,9
1,2,3,4,5,6,7,8,9

Note that .txt is the only valid extension. File without any extension will not be valid.
During this app execution it is possible to validate only one sudoku from one file.
If a file contains fewer or more rows, the sudoku will be marked as incorrect.
If any row contains fewer or more columns, the sudoku will be marked as incorrect.

To run the code it is needed to compile the code by maven first. 
mvn clean install

To generate reports in reports directory call:
mvn surefire-report:report

It can be run by executing validate.bat. Once run, this script will expect a sudoku file as an input. 
The file should be in the same directory as run.bat script.

This app can be further improved. However, it satisfies minimum requirements of a given exercise:
1. The program should return 0 (VALID) or non-zero (INVALID) value with an error text on stdout (in case of
   an invalid solution or file).
Comment: exits with code and prints the error message.

2. There should be unit tests covering a range of error conditions and the project should be maven or
   gradle based.

3. It should be possible to unpack the code from a zip, generate test report, build it and use a batch file to
   call the code from a packaged jar.